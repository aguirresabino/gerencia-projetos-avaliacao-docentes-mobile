(function(){
    'use strict';

    angular.module('AlunoController', ['AlunoService'])
        .controller('AlunoController', ['AlunoService', AlunoController]);

    function AlunoController(AlunoService){
        let vm = this;

        vm.cadastrar = function(aluno){
            AlunoService.create(aluno)
                .then(function(success){
                    console.log("Cadastro realizado com sucesso!");
                })
                .catch(function(error){
                    console.log("Cadastro não foi realizado: ", error);
                })
        }

        vm.ler = function(aluno){

        }

        vm.atualizar = function(aluno){
            AlunoService.update(aluno)
                .then(function(success){
                    console.log('Dados atualizados');
                })
                .catch(function(success){
                    console.log("Não foi possível atualizar os dados");
                })
        }

        vm.remover = function(aluno){

        }
    }

})();