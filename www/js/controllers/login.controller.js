(function(){
	'use strict';

	angular.module('LoginController', ['AlunoService'])
		.controller('LoginController', ['AlunoService', '$state', LoginController]);

	function LoginController(AlunoService, $state){
		let vm = this;

		vm.login = function(login){
			console.log(login);
			AlunoService.login(login)
				.then(function(success){
					let token = success.data.senha;
					localStorage.setItem('token', token);

					let alunoLogado = success.data;
					localStorage.setItem('alunoLogado', alunoLogado);

					console.log('Login realizado');

					$state.go('menu.ranking');
				})
				.catch(function(error){
					console.log("Erro login aluno: ", error);
				})
		}
	}
})();