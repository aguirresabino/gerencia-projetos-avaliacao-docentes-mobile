(function(){
	'use strict';

	angular.module('AvaliacaoController', ['AvaliacaoService', 'CriterioService'])
		.controller('AvaliacaoController', ['AvaliacaoService', 'CriterioService', AvaliacaoController]);

		function AvaliacaoController(AvaliacaoService, CriterioService){
			let vm = this;

			vm.professor = angular.fromJson(localStorage.professor);

			vm.create = function(avaliacao){

				avaliacao.pontuacoes = buildPontosAvaliativos();
				avaliacao.matProfessor = vm.professor.matricula;

				console.log(avaliacao);

				AvaliacaoService.create(avaliacao)
					.then((success) => {
						console.log("Avaliacao feita");
					})
					.catch((error) => {
						console.log('error', error);
					});
			};

			vm.pontosAvaliativos = {
				pessimo: 0,
				ruim: 2,
				regular: 4,
				bom: 6,
				otimo: 8,
				excelente: 10
			};

			let getCriterios = function(){
				CriterioService.read()
					.then((success) =>  {
						vm.criterios = success.data;
					})
					.catch((error) => {
						console.log('error', error);
					})
			}();

			let getCodCriterios = function(){
				let idsCriterios = [];
				for(let criterio of vm.criterios){
					idsCriterios.push(criterio.codigo);
				}
				return idsCriterios;
			};

			let getNotasOfCriterios = function(){
				let notas = [];
				$("input.nota").each(function(){
					notas.push(Number($(this).val()));
				});
				return notas;
			};

			let buildPontosAvaliativos = function(){
				let pontuacoes = [];
				//Buscando as notas para cada ponto avaliativo votado
				let notasCriterios = getNotasOfCriterios();
				let codigosCriterios = getCodCriterios();

				if(notasCriterios.length !== codigosCriterios.length){
					return "total de notas não coincide com o total de criterios votados";
				}

				for(let i = 0; i < codigosCriterios.length; i++){
					pontuacoes[i] = {pontuacao: notasCriterios[i], codCriterio: codigosCriterios[i]};
				}

				return pontuacoes;
			};
		}
})();