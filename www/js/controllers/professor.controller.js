(function(){
	'use strict';

	angular.module('ProfessorController', ['ProfessorService', 'RankingService'])
		.controller('ProfessorController', ['ProfessorService', 'RankingService', '$state', ProfessorController]);

	function ProfessorController(ProfessorService, RankingService, $state){
		let vm = this;

		let read = function(){
			ProfessorService.read()
				.then((success) => {
					console.log(success.data);
					vm.professores = success.data;
				})
				.catch((error) => {
					console.log('error', error);
				})
		}();

		let rankingProfGeral = function(){
			RankingService.rankingGeral()
				.then((success) => {
					console.log(success.data);
					vm.rankingGeral = success.data;
				})
				.catch((error) => {
					console.log('error', error);
				})
		}();

		/*let rankingProfSemetral = function(){
			RankingService.rankingSemetral()
				.then((success) => {
					console.log(success.data);
					vm.ranking = success.data;
				})
				.catch((error) => {
					console.log('error', error);
				})
		}();
*/


		vm.envia = function(professor){
			localStorage.professor = angular.toJson(professor);
			$state.go('menu.votar');
		}
	}
})();