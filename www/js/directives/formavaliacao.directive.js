(function(){
    "use strict";

    angular.module('FormAvaliacao', [])
        .directive('formAvaliacao', FormAvaliacao);
    
    function FormAvaliacao(){
        return {
            templateUrl : "templates/formavaliacao.html",
            controller: "AvaliacaoController",
            controllerAs: "Avaliacao"
        };    
    }
})();