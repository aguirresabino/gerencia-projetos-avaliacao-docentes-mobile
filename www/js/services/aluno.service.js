(function(){
	'use strict';

	angular.module('AlunoService', [])
		.service('AlunoService', AlunoService);

	function AlunoService($http){
		const urlBase = 'https://avaliacao-docentes.herokuapp.com/rest';
		const methods = {
			GET : 'GET',
			POST : 'POST',
			PUT : 'PUT',
			DELETE : 'DELETE'
		};

		this.login = function(login){
			let path = 'login/loginAluno';

			let request = {
				url: `${urlBase}/${path}`,
				data: $.param({'matricula': login.matricula, 'senha': login.senha}),
				method: methods.POST,
				headers: {'Content-Type' : 'application/x-www-form-urlencoded'}
			};

			return $http(request);
		}

		/*this.create = function(aluno){
			let path = 'aluno/cadastrarAluno';

			let request = {
				url: `${urlBase}/${path}`,
				method: methods.POST,
				data: aluno		
			};

			return $http(request);
		}*/

		/*this.read = function(aluno){

		}*/

		this.update = function(aluno){
			let path = 'aluno/editarConta';

			let request = {
				url: `${urlBase}/${path}`,
				method: methods.POST,
				data: aluno		
			};

			return $http(request);
		}

		/*this.delete = function(id){
		
		}*/
	}
	AlunoService.$inject = ['$http'];
})();