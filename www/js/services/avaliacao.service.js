(function(){

	'user strict';

	angular.module('AvaliacaoService', [])
		.service('AvaliacaoService', AvaliacaoService);

	function AvaliacaoService($http){
		const urlBase = 'https://avaliacao-docentes.herokuapp.com/rest';
		const methods = {
			GET : 'GET',
			POST : 'POST',
			PUT : 'PUT',
			DELETE : 'DELETE'
		};

		this.create = function(avaliacao){
			let path = 'aluno/enviarAvaliacao';

			let request = {
				url: `${urlBase}/${path}`,
				method: methods.POST,
				data: avaliacao,		
				headers: {'Authorization': 'Bearer '+ localStorage.token}
			};

			return $http(request);
		}

		this.read = function(){
			//implementa
		}
	}
	AvaliacaoService.$inject = ['$http'];
})();