(function(){

	'use strict';

	angular.module('ProfessorService', [])
		.service('ProfessorService', ProfessorService);

	function ProfessorService($http){
		const urlBase = 'https://avaliacao-docentes.herokuapp.com/rest';
		const methods = {
			GET : 'GET',
			POST : 'POST',
			PUT : 'PUT',
			DELETE : 'DELETE'
		};

		this.create = function(avaliacao){
			//implentar
		}
		
		this.read = function(){
			let path = 'professor/listarProfessores';

			let request = {
				url: `${urlBase}/${path}`,
				method: methods.GET,
				headers: {'Authorization': 'Bearer '+ localStorage.token}
			};

			return $http(request);
		}
	}
	ProfessorService.$inject = ['$http'];
})();