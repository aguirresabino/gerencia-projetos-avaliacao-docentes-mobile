(function(){
	'use strict';

	angular.module('RankingService', [])
		.service('RankingService', RankingService);

	function RankingService($http){
		const urlBase = 'https://avaliacao-docentes.herokuapp.com/rest';
		const methods = {
			GET : 'GET',
			POST : 'POST',
			PUT : 'PUT',
			DELETE : 'DELETE'
		};

		this.rankingGeral = function(){
			let path = 'ranking/rankingGeral';

			let request = {
				url: `${urlBase}/${path}`,
				method: methods.GET,
				headers: {'Authorization': 'Bearer '+ localStorage.token}
			};

			return $http(request);
		}

		this.rankingSemetral = function(){
			let path = 'ranking/rankingSemestral';

			let request = {
				url: `${urlBase}/${path}`,
				method: methods.GET
			};

			return $http(request);
		}
	}

	RankingService.$inject = ['$http'];
})();