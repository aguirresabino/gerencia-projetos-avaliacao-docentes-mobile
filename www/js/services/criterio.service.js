(function(){
	'use strict';

	angular.module('CriterioService', [])
		.service('CriterioService', CriterioService)

	function CriterioService($http){
		const urlBase = 'https://avaliacao-docentes.herokuapp.com/rest';
		const methods = {
			GET : 'GET',
			POST : 'POST',
			PUT : 'PUT',
			DELETE : 'DELETE'
		};

		this.create = function(criterio){
			//implementar
		}

		this.read = function(){
			let path = 'criterio/listarCriterios';

			let request = {
				url: `${urlBase}/${path}`,
				method: methods.GET
			};

			return $http(request);
		}
	}
	CriterioService.$inject = ['$http'];
})();