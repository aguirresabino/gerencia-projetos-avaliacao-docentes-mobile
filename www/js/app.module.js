(function(){
  'use strict';

  angular.module('app', ['ionic', 'LoginController', 'AlunoController', 'AvaliacaoController', 'CriterioController', 'ProfessorController', 'FormAvaliacao'])
      .config(conf)
      .run(preAtivador)
      /*
        This directive is used to disable the "drag to open" functionality of the Side-Menu
        when you are dragging a Slider component.
      */
      .directive('disableSideMenuDrag', ['$ionicSideMenuDelegate', '$rootScope', function($ionicSideMenuDelegate, $rootScope) {
        return {
            restrict: "A",  
            controller: ['$scope', '$element', '$attrs', function ($scope, $element, $attrs) {

                function stopDrag(){
                  $ionicSideMenuDelegate.canDragContent(false);
                }

                function allowDrag(){
                  $ionicSideMenuDelegate.canDragContent(true);
                }

                $rootScope.$on('$ionicSlides.slideChangeEnd', allowDrag);
                $element.on('touchstart', stopDrag);
                $element.on('touchend', allowDrag);
                $element.on('mousedown', stopDrag);
                $element.on('mouseup', allowDrag);

            }]
        };
      }])
      /*
      This directive is used to open regular and dynamic href links inside of inappbrowser.
      */
      .directive('hrefInappbrowser', function() {
      return {
        restrict: 'A',
        replace: false,
        transclude: false,
        link: function(scope, element, attrs) {
          var href = attrs['hrefInappbrowser'];

          attrs.$observe('hrefInappbrowser', function(val){
            href = val;
          });
          
          element.bind('click', function (event) {

            window.open(href, '_system', 'location=yes');

            event.preventDefault();
            event.stopPropagation();

          });
        }
      };
      });

    function preAtivador($ionicPlatform) {
      $ionicPlatform.ready(function() {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
          cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
          cordova.plugins.Keyboard.disableScroll(true);
        }
        if (window.StatusBar) {
          // org.apache.cordova.statusbar required
          StatusBar.styleDefault();
        }
      });
    }

  function conf($ionicConfigProvider, $sceDelegateProvider){
    $sceDelegateProvider.resourceUrlWhitelist([ 'self','*://www.youtube.com/**', '*://player.vimeo.com/video/**']);
  }
  
  preAtivador.$inject = ['$ionicPlatform'];
  conf.$inject = ['$ionicConfigProvider', '$sceDelegateProvider'];
})();