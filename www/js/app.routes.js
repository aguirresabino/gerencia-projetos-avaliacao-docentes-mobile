(function(){
	'use strict';

	angular.module('app')
		.config(config)

	function config($stateProvider, $urlRouterProvider) {			
		$stateProvider
		.state('menu', {
			url: '/side-menu21',
			templateUrl: 'templates/menu.html',
			/* controller: 'menuCtrl' */
		})

		.state('menu.avaliarDocente', {
			url: '/avaliacao',
			views: {
			'side-menu21': {
				templateUrl: 'templates/professores.html',
				controller: 'ProfessorController',
				controllerAs: 'Professor'
			}
			}
		})
		
		.state('menu.votar', {
			url: '/votacao',
			views: {
			'side-menu21': {
				templateUrl: 'templates/avalia.professor.html',
				controller: 'AvaliacaoController',
				controllerAs: 'Avaliacao'
			}
			}
		})
		
		.state('menu.ranking', {
			url: '/ranking',
			views: {
			'side-menu21': {
				templateUrl: 'templates/ranking.html',
				controller: 'ProfessorController',
				controllerAs: 'Professor'
			}
			}
		})
		.state('menu.perfil', {
			url: '/perfil',
			views: {
			'side-menu21': {
				templateUrl: 'templates/perfil.html',
				controller: 'AlunoController',
				controllerAs: 'Aluno'
			}
			}
		})
		
		.state('login', {
			url: '/login',
			templateUrl: 'templates/login.html',
			controller: 'LoginController',
			controllerAs: 'Login'
		})
		
		$urlRouterProvider.otherwise('/login')  
	}

	/* function config($routeProvider){
		$routeProvider
		    .when('/login', {
		    	templateUrl: '../views/login.html',
		    	controller: 'LoginController',
		    	controllerAs: 'Login'
			})
			.when('/aluno-principal', {
				templateUrl: '../views/aluno-principal.html',
				controller: 'AlunoController',
				controllerAs: 'Aluno'
			})
			.when('/avaliar-professor', {
				templateUrl: '../views/avaliar-professor.html',
				controller: 'AvaliacaoController',
				controllerAs: 'Avaliacao'
			})
		    .otherwise({
		        redirectTo: '/login'
		    })
	} */
	config.$inject = ['$stateProvider','$urlRouterProvider'];
})();